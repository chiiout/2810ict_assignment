import re
def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, list):
  return [word for word in words
                 if re.match(pattern, word) and word not in seen.keys() and
                    word not in list]

def find(word, words, seen, target, path):
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  if len(list) == 0:
    return False
  if shortest == 'yes':
    list = sorted([(same(w, target), w) for w in list],reverse=True)
  else:
    list = sorted([(same(w, target), w) for w in list])
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        path.append(item)
      return True
    seen[item] = True
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True
    path.pop()

fname = input("Enter dictionary name: ")
try:
  file = open(fname)
except:
  print('file name not found')
  exit()

lines = file.readlines()
while True:
  # forbidden words ,list end until -1 is input
  fword = []
  while True:
    a = input('enter forbidden word: ')
    fword.append(a)
    if a == '-1':
      fword.pop()
      break
  print(fword)

  while True:
    start = input("Enter start word:")
    if re.match("^[a-z]*$", start):
      break
    else:
      print('Error! Only letters a-z allowed!')
  words = []
  for line in lines:
    word = line.rstrip()
    # add condition for not adding forbidden words into list
    if len(word) == len(start) and word not in fword:
      words.append(word)
  while True:
    target = input("Enter target word:")
    if re.match("^[a-z]*$", target):
      break
    else:
      print('Error! Only letters a-z allowed!')
  break

shortest = input('Enter yes to use the shortest path ')
count = 0
path = [start]
seen = {start : True}
if find(start, words, seen, target, path):
  path.append(target)
  print(len(path) - 1, path)
else:
  print("No path found")

