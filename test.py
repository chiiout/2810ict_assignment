import re
import unittest

def same(item, target):
  return len([c for (c, t) in zip(item, target) if c == t])

def build(pattern, words, seen, lists):
  return [word for word in words
                 if re.match(pattern, word) and word not in seen.keys() and
                    word not in lists]

def find(word, words, seen, target, path):
  list = []
  for i in range(len(word)):
    list += build(word[:i] + "." + word[i + 1:], words, seen, list)
  list = sorted([(same(w, target), w) for w in list], reverse=True)
  for (match, item) in list:
    if match >= len(target) - 1:
      if match == len(target) - 1:
        return True
  for (match, item) in list:
    path.append(item)
    if find(item, words, seen, target, path):
      return True


class WordLadderTest(unittest.TestCase):

    def testSame(self):
        self.assertEqual(same('hide', 'seek'),0)

    def testBuild(self):
        self.assertNotIn(build('.ead',words ,seen, lists), ['lead'])

    def testFind(self):
        self.assertTrue(find('lead',words, seen,'gold',path))

def main():
    unittest.main()
file = open('dictionary.txt')
lines = file.readlines()
words = []
for line in lines:
    word = line.rstrip()
    if len(word) == 4 :
      words.append(word)

lists = ['aahs', 'aals', 'abas', 'abba', 'abbe', 'abed', 'abet']
path = ['lead']
seen = {'lead' : True}

if __name__ == '__main__':
    main()


